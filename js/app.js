const email = document.querySelector('#email');
const asunto = document.querySelector('#asunto');
const mensaje = document.querySelector('#mensaje');
const btnEnviar = document.querySelector('#enviar');
const formEnviar = document.querySelector('#enviar-mail');
const resetBtn = document.querySelector('#resetBtn');

eventListeners();

function eventListeners() {
    document.addEventListener('DOMContentLoaded', inicioApp);

    email.addEventListener('blur', validarCampo);
    asunto.addEventListener('blur', validarCampo);
    mensaje.addEventListener('blur', validarCampo);

    formEnviar.addEventListener('submit', enviarEmail);

    resetBtn.addEventListener('click', limpiarForm);
}

function inicioApp() {
    btnEnviar.disabled = true;
    removeValidacionInput(email);
    removeValidacionInput(asunto);
    removeValidacionInput(mensaje);
}

function removeValidacionInput(campo) {
    const label = campo.nextElementSibling;
    campo.style.borderBottomColor = '#9e9e9e';
    campo.classList.remove('error');
    label.style.color = '#9e9e9e';
}

function validarCampo() {
    validarLongitud(this);

    if (this.type === 'email') {
        validarEmail(this);
    }

    const getValues = email.value !== '' && asunto.value !== '' && mensaje.value !== '';

    const errors = document.querySelectorAll('.error');

    btnEnviar.disabled = getValues && errors.length === 0 ? false : true;
}

function validarLongitud(campo){
    const label = campo.nextElementSibling;
    if(campo.value.length > 0) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
        label.style.color = 'green';
        
    } else {
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
        label.style.color = 'red';
    }
}

function validarEmail(campo) {
    const input = campo.value;
    const label = campo.nextElementSibling;

    if (input.indexOf('@') !== -1 && input.indexOf('.') !== -1) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
        label.style.color = 'green';

    } else {
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
        label.style.color = 'red';
    }
}

function enviarEmail(e) {

    const loaders = document.querySelector('#loaders');
    
    const spinnerGIF = document.querySelector('#spinner');
    spinnerGIF.style.display = 'block';

    const enviado = document.createElement('img');
    enviado.src = 'img/mail.gif';
    enviado.style.display = 'block';

    setTimeout(() => {
        spinnerGIF.style.display = 'none';
        loaders.appendChild(enviado);
        limpiarForm();

        setTimeout(() => {
            enviado.remove();
        }, 2000);
    }, 2000);

    e.preventDefault();
}

function limpiarForm() {
    formEnviar.reset();
    inicioApp();
}